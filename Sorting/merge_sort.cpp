/* Merge sort
- A sorting algorithm follow the divide-and-conquer approach:
    + Divide the problem into a number of subproblems that are smaller
        instances of the same problem.
    + Conquer the subproblems by solving them recursively. If the
        subproblem sizes are small enough, just solve the subproblems
        in a straightforward manner.
    + Combine the solutions to the subproblems => the solution for
        the original problem.

- Operate:
    + Divide: Divide the n-element sequence to be sorted into two subsequences
        of n=2 elements each.
    + Conquer: Sort the two subsequences recursively using merge sort.
    + Combine: Merge the two sorted subsequences to produce the sorted answer.

- Time complexity:
    + Worst-case: O(N lg(N))
    + Average-case: O(N lg(N))
*/

#include<iostream>
using namespace std;

void merge(int *array, int start, int mid, int end, bool ascending)
{
    // Merge 2 sorted sub-arrays into 1 sorted array
    int l1 = mid - start + 1;
    int l2 = end - mid;
    int left[l1], right[l2];
    for(int i = 0; i < l1; i++)
        left[i] = array[start + i];
    for(int i = 0; i < l2; i++)
        right[i] = array[mid + i + 1];

    int ind = start, left_ind = 0, right_ind = 0;
    if(ascending)
        while(left_ind != l1 && right_ind != l2)
        {
            if(left[left_ind] < right[right_ind])
                array[ind++] = left[left_ind++];
            else
                array[ind++] = right[right_ind++];
        }
    else
        while(left_ind != l1 && right_ind != l2)
        {
            if(left[left_ind] > right[right_ind])
                array[ind++] = left[left_ind++];
            else
                array[ind++] = right[right_ind++];
        }
    if(left_ind == l1)
        while(right_ind != l2)
            array[ind++] = right[right_ind++];
    else
        while(left_ind != l1)
            array[ind++] = left[left_ind++];
}

void merge_sort(int *array, int start, int end, bool ascending)
{
    // Split current array into 2 sub-arrays
    if(start < end)
    {   
        int mid = (start + end) / 2;
        merge_sort(array, start, mid, ascending);
        merge_sort(array, mid + 1, end, ascending);
        merge(array, start, mid, end, ascending);
    }
}

void merge_sort(int *array, int n, bool ascending)
{
    merge_sort(array, 0, n - 1, ascending);
}

void print_result(int *array, int n)
{
    for(int i = 0; i < n; i++)
        cout<<array[i]<<" ";
}

int main()
{
    int n = 10;
    int array[n] = {15, 22, 89, 11, 32, 68, 89, 83, 77, 29};
    merge_sort(array, n, false);
    print_result(array, n);
    return 1;
}