/* Quick sort
- A sorting algorithm follow the divide-and-conquer approach.
- Sort in-place.
- Operate:
    + Divide: Partition the array A[p..r] into two subarrays A[p..q-1]
        and A[q+1..r] such that each element of A[p..q-1] is less than
        or equal to A[q], which is, in turn, less than or equal to each
        element of A[q+1..r].
    + Conquer: Sort the two subarrays A[p..q-1] and A[q+1..r] by recursive
        calls to quicksort.
    + Combine: Because the subarrays are already sorted, no work is needed
        to combine them: the entire array A[p..r] is now sorted.
- Time complexity:
    + Worst-case: O(N^2)
    + Average-case: O(N lg(N))
*/

#include<iostream>
#include <random>
using namespace std;

int random(int low, int high)
{
    mt19937 rng;
    rng.seed(random_device()());
    uniform_int_distribution<mt19937::result_type> dist6(low, high);
    return dist6(rng);
}

int partition(int* array, int start, int end, bool ascending)
{
    int pivot = random(start, end);
    swap(array[pivot], array[end]);
    int x = array[end];
    int q = start;
    if(ascending)
        for(int j = start; j < end; j++)
        {
            if(array[j] <= x)
                swap(array[q++], array[j]);
        }
    else
        for(int j = start; j < end; j++)
        {
            if(array[j] >= x)
                swap(array[q++], array[j]);
        }
    swap(array[q], array[end]);
    return q;
}

void quick_sort(int* array, int start, int end, bool ascending)
{
    if(start < end)
    {
        int q = partition(array, start, end, ascending);
        quick_sort(array, start, q - 1, ascending);
        quick_sort(array, q + 1, end, ascending);
    }
}

void quick_sort(int* array, int n, bool ascending)
{
    quick_sort(array, 0, n - 1, ascending);
}

void print_result(int *array, int n)
{
    for(int i = 0; i < n; i++)
        cout<<array[i]<<" ";
}

int main()
{
    int n = 10;
    int array[] = {15, 22, 89, 11, 32, 68, 89, 83, 77, 29};
    quick_sort(array, n, true);
    print_result(array, n);
    return 1;
}