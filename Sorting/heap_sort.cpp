/* Heap sort
- A sorting algorithm based on heap data structure:
    + An nearly complete binary tree that obeys the max-heap 
        (or min-heap) property: Every subtrees has the root value larger
        (or smaller) than its children.
    + Sort in-place.
- Operate:
    + Build max-heap structure of the input array A[1..n].
    + Repeat from n to 2
    +   Swap the root value A[0] with A[n] and discard node n from the heap.
    +   Restore the max-heap property in the root node.
- Time complexity:
    + Worst-case: O(N lg(N))
    + Average-case: O(N lg(N))
*/

#include<iostream>
using namespace std;

int parent(int i)
{
    return (i - 1) / 2;
}

int left(int i)
{
    return 2 * i + 1;
}

int right(int i)
{
    return 2 * i + 2;
}

void max_heapify(int* array, int n, int i)
{
    // Restore the max-heap property in node i
    int l = left(i), r = right(i), largest = i;
    if(l < n && array[l] > array[i])
        largest = l;
    if(r < n && array[r] > array[largest])
        largest = r;
    if(largest != i)
    {
        swap(array[i], array[largest]);
        max_heapify(array, n, largest);
    }
}

void min_heapify(int* array, int n, int i)
{
    int l = left(i), r = right(i), smallest = i;
    if(l < n && array[l] < array[i])
        smallest = l;
    if(r < n && array[r] < array[smallest])
        smallest = r;
    if(smallest != i)
    {
        swap(array[i], array[smallest]);
        min_heapify(array, n, smallest);
    }
}

void build_max_heap(int* array, int n)
{
    for(int i = n / 2 - 1; i >= 0; i--)
        max_heapify(array, n, i);
}

void build_min_heap(int* array, int n)
{
    for(int i = n / 2 - 1; i >= 0; i--)
        min_heapify(array, n, i);
}

void heap_sort(int* array, int n, bool ascending)
{
    int count = n;
    if(ascending)
    {
        build_max_heap(array, n);
        for(int i = n - 1; i >= 1; i--)
        {
            swap(array[0], array[i]);
            count -= 1;
            max_heapify(array, count, 0);
        }
    }
    else
    {
        build_min_heap(array, n);
        for(int i = n - 1; i >= 1; i--)
        {
            swap(array[0], array[i]);
            count -= 1;
            min_heapify(array, count, 0);
        }
    }
}

void print_result(int *array, int n)
{
    for(int i = 0; i < n; i++)
        cout<<array[i]<<" ";
}

int main()
{
    int n = 10;
    int array[] = {15, 22, 89, 11, 32, 68, 89, 83, 77, 29};
    heap_sort(array, n, true);
    print_result(array, n);
    return 1;
}