#include <random>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void write_to_file(int* seq, int seq_length)
{
    string file_name = "data/seq" + to_string(seq_length) + ".txt";
    ofstream outfile;
    outfile.open(file_name, ios::app);
    outfile << seq_length << endl;
    for(int i = 0; i < seq_length - 1; i++)
        outfile << seq[i] << " ";
    outfile << seq[seq_length - 1] << endl;
    outfile.close();
}

void generate_random_sequences(int* seq, int length)
{
    mt19937 rng;
    rng.seed(random_device()());
    uniform_int_distribution<mt19937::result_type> dist6(1, 100);
    int i = 0;
    while(i < length)
        seq[i++] = dist6(rng);
}

void generate_test_cases()
{
    int cases[] = {10, 100, 1000};
    for(int t = 0; t < 3; t++)
    {
        int seq[cases[t]];
        for(int i = 0; i < 50; i++)
        {
            generate_random_sequences(seq, cases[t]);
            write_to_file(seq, cases[t]);
        }
    }
}

int main()
{
    generate_test_cases();
    return 1;
}