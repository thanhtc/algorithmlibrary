/* Radix sort
- Assumes that each element in the n-element array A has d digits.
- Operate:
    + Sort the array from the least significant digit to the most significant
        digit using stable sort algorithm.
- Time complexity:
    + Given n d-digit numbers in which each digit can take on up to k
        possible values: O(d(n + k)) if the stable sort take O(n + k).
*/
#include<iostream>
#include<string>
using namespace std;

void copy_array(string* a, string* b, int n)
{
    for(int i = 0; i < n; i++)
        a[i] = b[i];
}

void counting_sort(string* array, int n, int p, bool ascending)
{
    int k = 10;
    int count[k];
    string result[n];
    for(int i = 0; i < k; i++)
        count[i] = 0;
    for(int i = 0; i < n; i++)
    {
        int t = (int)array[i][p] - 48;
        count[t] += 1;
    }
    for(int i = 1; i < k; i++)
    {
        count[i] += count[i - 1];
    }
    if(ascending)
        for(int i = n - 1; i >= 0; i--)
        {
            int t = (int)array[i][p] - 48;
            result[count[t] - 1] = array[i];
            count[t] -= 1;
        }
    else
        for(int i = 0; i < n; i++)
        {
            int t = (int)array[i][p] - 48;
            result[n - count[t]] = array[i];
            count[t] -= 1;
        }
    copy_array(array, result, n);
}

void radix_sort(int* array, int n, int d, bool ascending)
{
    string str_array[n];
    for(int i = 0; i < n; i++)
        str_array[i] = to_string(array[i]);

    for(int i = d - 1; i >=0; i--)
    {
        counting_sort(str_array, n, i, ascending);
    }
    for(int i = 0; i < n; i++)
    {
        array[i] = stoi(str_array[i]);
    }
}

void print_result(int *array, int n)
{
    for(int i = 0; i < n; i++)
        cout<<array[i]<<" ";
}

int main()
{
    int n = 10, d = 2;
    int array[n] = {15, 22, 89, 11, 32, 68, 89, 83, 77, 29};
    radix_sort(array, n, d, true);
    print_result(array, n);
    return 1;
}