/* Counting sort
- Assumes that that each of the n input elements is an integer in the
    range 0 to k, for some integer k.
- Stable: Numbers with the same value appear in the output array in the same
    order as they do in the input array.
- Operate:
    + Determines for each input element x, the number of elements less
        than x.
    + Place element x directly into its position in the output array with
        its position is the number of elements lesser than x.
- Time complexity:
    + Given n numbers and a upper bound k : O(n + k).
*/

#include<iostream>
using namespace std;

void copy_array(int* a, int* b, int n)
{
    for(int i = 0; i < n; i++)
        a[i] = b[i];
}

void counting_sort(int* array, int n, int k, bool ascending)
{
    int count[k], result[n];
    for(int i = 0; i < k; i++)
        count[i] = 0;
    for(int i = 0; i < n; i++)
        count[array[i]] += 1;
    for(int i = 1; i < k; i++)
        count[i] += count[i - 1];
    if(ascending)
        for(int i = n - 1; i >= 0; i--)
        {
            result[count[array[i]] - 1] = array[i];
            count[array[i]] -= 1;
        }
    else
        for(int i = 0; i < 0; i++)
        {
            result[n - count[array[i]]] = array[i];
            count[array[i]] -= 1;
        }
    copy_array(array, result, n);
}

void print_result(int *array, int n)
{
    for(int i = 0; i < n; i++)
        cout<<array[i]<<" ";
}

int main()
{
    int n = 10, k = 100;
    int array[n] = {15, 22, 89, 11, 32, 68, 89, 83, 77, 29};
    counting_sort(array, n, k, true);
    print_result(array, n);
    return 1;
}

