/* Stack
- A data structure with the LIFO policy.
- Time complexity:
    + Push / Pop: O(1)
*/
#include<iostream>
using namespace std;

class Stack
{
    int n;
    int top;
    int* elements;
    public:
        Stack(int);
        Stack(int, int*);
        void push(int);
        int pop();
        bool empty();
};

Stack::Stack(int n)
{
    this->n = n;
    this->top = -1;
    this->elements = new int[n];
}

Stack::Stack(int n, int* array)
{
    this->n = n;
    this->top = n - 1;
    this->elements = array;
}

bool Stack::empty()
{
    return top == -1;
}

void Stack::push(int x)
{
    if(top == n)
    {
        cout<< "Stack overflow" << endl;
        throw;
    }
    elements[++top] = x;
}

int Stack::pop()
{
    if(empty())
    {
        cout << "Stack underflow" << endl;
        throw;
    }
    return elements[top--];
}

int main()
{
    int n = 5;
    int array[] = {15, 22, 89, 11, 32};
    Stack stack = Stack(n, array);
    int last = stack.pop();
    cout << "Last element: " << last << endl;
    stack.push(100);
    last = stack.pop();
    cout << "Last element: " << last << endl;
    return 1;
}