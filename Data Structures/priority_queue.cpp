/* Priority Queue
- A queue with priority property based on heap data structure.
- Each elements will be processed in order of priority.
- Time complexity:
    + Init: O(N lg (N))
    + Add / change element priority: O(lg (N))
    + Extract element with max priority: O(lg (N))
*/

#include<iostream>
using namespace std;

class Job
{
    public:
        int priority;
        char label;
        Job();
        Job(int, char);
};

Job::Job()
{
    this->priority = NULL;
    this->label = NULL;
}

Job::Job(int p, char l)
{
    this->priority = p;
    this->label = l;
}

class PriorityQueue
{
    int capacity;
    int heap_size;
    Job* job_list;
    public:
        PriorityQueue(int, Job*);
        Job get_job(int);
        int add_job(Job);
        Job get_maximum_priority_job();
        void build_max_heap();
        void max_heapify(int);
        Job extract_max();
        int change_priority(int, int);
        void log_info();
};

PriorityQueue::PriorityQueue(int size, Job* job_list)
{
    this->capacity = size;
    this->heap_size = size;
    this->job_list = job_list;
    build_max_heap();
}

Job PriorityQueue::get_job(int ind)
{
    if(ind >= heap_size)
        return Job();
    return this->job_list[ind];
}

int PriorityQueue::add_job(Job job)
{
    if(heap_size >= capacity)
        return -1;
    int priority = job.priority;
    job_list[heap_size++] = job;
    change_priority(heap_size - 1, priority);
    return 1; 
}

int PriorityQueue::change_priority(int ind, int new_priority)
{
    if(ind < 0 || ind >= heap_size)
        return -1;
    job_list[ind].priority = new_priority;
    // Maintain the max heap property in queue
    while(ind > 0 && job_list[ind].priority > job_list[(ind - 1) / 2].priority)
    {
        Job tmp = job_list[ind];
        job_list[ind] = job_list[(ind - 1) / 2];
        job_list[(ind - 1) / 2] = tmp;
        ind = (ind - 1) / 2;
    }
    return 1;
}

Job PriorityQueue::get_maximum_priority_job()
{
    return job_list[0];
}

void PriorityQueue::max_heapify(int ind)
{
    int largest = ind, left = 2 * ind + 1, right = 2 * ind + 2;
    if(left < heap_size && job_list[left].priority >
            job_list[largest].priority)
        largest = left;
    if(right < heap_size && job_list[right].priority >
            job_list[largest].priority)
        largest = right;
    if(largest != ind)
    {
        Job tmp = job_list[largest];
        job_list[largest] = job_list[ind];
        job_list[ind] = tmp;
        max_heapify(largest);
    }
}

void PriorityQueue::build_max_heap()
{
    for(int i = heap_size / 2 - 1; i >= 0; i--)
        max_heapify(i);
}

Job PriorityQueue::extract_max()
{
    if(heap_size < 1)
        return Job();
    Job max_priority = job_list[0];
    job_list[0] = job_list[heap_size - 1];
    heap_size -= 1;
    max_heapify(0);
    return max_priority;
}

void PriorityQueue::log_info()
{
    for(int i = 0; i < heap_size; i++)
    {
        cout << job_list[i].label << " - " << job_list[i].priority << " | ";
    }
    cout << endl;
}

int main()
{
    int n = 5;
    int priority[] = {15, 22, 89, 11, 32};
    char jobs[] = {'A', 'B', 'C', 'D', 'E'}; 
    Job job_list[n];
    for(int i = 0; i < 5; i++)
    {
        job_list[i] = Job(priority[i], jobs[i]);
    }
    cout << "Init queue: " << endl;
    PriorityQueue queue = PriorityQueue(n, job_list);
    queue.log_info();
    Job max_priority = queue.extract_max();
    cout << "Extract max priority job: " << max_priority.label << " - " <<
        max_priority.priority << endl;
    queue.log_info();
    cout << "Add job F with priority 100" << endl;
    queue.add_job(Job(100, 'F'));
    queue.log_info();
    return 1;
}