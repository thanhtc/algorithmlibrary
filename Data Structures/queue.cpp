/* Queue
- A data structure with the FIFO policy.
- Time complexity:
    + Enqueue / Dequeue: O(1)
*/
#include<iostream>
using namespace std;

class Queue
{
    int n;
    int head;
    int tail;
    int* elements;
    public:
        Queue(int);
        Queue(int, int*);
        void enqueue(int);
        int dequeue();
        bool empty();
};

Queue::Queue(int n)
{
    this->n = n + 1;
    this->head = this->tail = 0;
    this->elements = new int[n + 1];
}

Queue::Queue(int n, int* array)
{
    this->n = n + 1;
    this->head = 0;
    this->tail = n;
    this->elements = new int[n + 1];
    for(int i = 0; i < n; i++)
        elements[i] = array[i];
    elements[n] = 0;
}

bool Queue::empty()
{
    return head == tail;
}

void Queue::enqueue(int x)
{
    int next = tail + 1;
    if(next == n)
        next = 0;
    if(next == head)
    {
        cout << "Stack overflow" << endl;
        throw;
    }
    else
    {
        elements[tail] = x;
        tail = next;
    }
}

int Queue::dequeue()
{
    if(empty())
    {
        cout << "Stack underflow" << endl;
        throw;
    }
    else
    {
        int current_head = elements[head];
        int next = head + 1;
        if(next == n)
            next = 0;
        head = next;
        return current_head;
    }
}

int main()
{
    int n = 5;
    Queue queue = Queue(n);
    queue.enqueue(1);
    int first = queue.dequeue();
    cout << "First element: " << first << endl;
    queue.enqueue(2);
    first = queue.dequeue();
    cout << "First element: " << first << endl;
    return 1;
}